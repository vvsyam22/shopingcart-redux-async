# ShopingCart-redux-async

It's sample shopping cart react native project. Developed using redux and asynchronous storage.
**SCREENSHOTS**::


![Simulator_Screen_Shot_-_iPhone_11_-_2020-10-18_at_12.39.36](/uploads/61a7e4dd9cc85562ec119fc18a754d19/Simulator_Screen_Shot_-_iPhone_11_-_2020-10-18_at_12.39.36.png)
![Simulator_Screen_Shot_-_iPhone_11_-_2020-10-18_at_13.03.20](/uploads/d46c407808f0fe7aef71ca54d6669350/Simulator_Screen_Shot_-_iPhone_11_-_2020-10-18_at_13.03.20.png)
![Simulator_Screen_Shot_-_iPhone_11_-_2020-10-18_at_12.39.41](/uploads/255a034e9d178f4933ce3947ed0b0b3f/Simulator_Screen_Shot_-_iPhone_11_-_2020-10-18_at_12.39.41.png)
![Simulator_Screen_Shot_-_iPhone_11_-_2020-10-18_at_12.39.56](/uploads/c414427f9cb31083c47be1207de6db0c/Simulator_Screen_Shot_-_iPhone_11_-_2020-10-18_at_12.39.56.png)

